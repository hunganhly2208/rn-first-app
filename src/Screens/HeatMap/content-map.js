import React, { useState,useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import _MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Heatmap } from 'react-native-maps'
import { selectHeatMapData, selectLoading } from '../../Store/covidSlice'


const ContentMap = () => {
  const dispatch = useDispatch();
  const res = useSelector(selectHeatMapData)

  const [points] = useState(
    res && res.filter((data) => {
      // console.log('data', data)
      return data ?.latitude && data ?.longitude && data ?.weight
  })
  )
  useEffect(() => {
    console.log('points', points)
    
  }, [points])
  return (
    true && (
      <Heatmap
        points={points}
        opacity={0.6}
        radius={50}
        gradient={{
          colors: [
            '#ff806b',
            '#ff806b',
            '#ff806b',
            '#ff806b',
            '#ff806b',
          ],
          startPoints: [0.01, 0.25, .5, .75, 1],
          colorMapSize: 100
        }}
        gradientSmoothing={10}
        heatmapMode={'POINTS_DENSITY'}
      />
    )
  )
}

export default ContentMap
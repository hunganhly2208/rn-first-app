import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import MapView from './map-view';

const HeatMap = () => {
  return (
    <View style={{ flex: 1 }}> 
      <MapView />
    </View>
  )
}

export default HeatMap
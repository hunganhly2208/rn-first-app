import React, { useState, Fragment, useEffect } from 'react';
import { View, StyleSheet, Keyboard, Platform, TouchableWithoutFeedback } from 'react-native';
import { Text, Layout, Select, SelectItem, Autocomplete, AutocompleteItem } from '@ui-kitten/components';
import { useSelector, useDispatch, batch } from 'react-redux';
// import { Ionicons } from '@expo/vector-icons';
import { globalStyles } from '../../../StylesGlobal'
import { styles } from './styles'
import {
  selectQueries,
  getCountries,
  getCountry,
  getCountriesFiltered
} from '../../../Store/covidSlice';

const filter = (item, query) => item.title.toLowerCase().includes(query.toLowerCase());

const Search = () => {
  const [placement, setPlacement] = useState('bottom');
  const [displayValue, setDisplayValue] = useState(0);
  const dispatch = useDispatch();

  const queries = useSelector(selectQueries);

  const dataSelect = [
    {
      title: 'Cases',
      filter: 'cases',
    },
    {
      title: 'Deaths',
      filter: 'deaths',
    },
    {
      title: 'New Cases',
      filter: 'todayCases',
    },
    {
      title: 'New Deaths',
      filter: 'todayDeaths',
    },
    {
      title: 'Active',
      filter: 'active',
    },
    {
      title: 'Recovered',
      filter: 'recovered',
    },
    {
      title: 'Critical',
      filter: 'critical',
    },
    {
      title: 'Cases Per One Million',
      filter: 'casesPerOneMillion',
    },
    {
      title: 'Deaths Per One Million',
      filter: 'deathsPerOneMillion',
    },
  ];

  const showEvent = Platform.select({
    android: 'keyboardDidShow',
    default: 'keyboardWillShow',
  });

  const hideEvent = Platform.select({
    android: 'keyboardDidHide',
    default: 'keyboardWillHide',
  });



  useEffect(() => {
    const keyboardShowListener = Keyboard.addListener(showEvent, () => {
      setPlacement('top');
    });

    const keyboardHideListener = Keyboard.addListener(hideEvent, () => {
      setPlacement('bottom');
    });

    return () => {
      keyboardShowListener.remove();
      keyboardHideListener.remove();
    };
  });

  const onSelect1 = (index) => {
    setDisplayValue(dataSelect[index.section].title)
    const filter = dataSelect[index.section].filter;
    dispatch(getCountriesFiltered(filter))
  }


  const [value2, setValue2] = React.useState(null);
  const [data2, setData2] = React.useState(queries);

  const onSelect2 = (index) => {
    const title = data2[index].title;
    setValue2(data2[index].title);
    if (title === 'All') {
      batch(() => {
        dispatch(getCountry(null));
        dispatch(getCountries);
      });
    } else {
      dispatch(getCountry(title));
    }
  };

  const clearInput2 = () => {
    setValue2('');
    setData2(queries);
    batch(() => {
      dispatch(getCountry(null));
      dispatch(getCountries)
    })
  };

  const onChangeText2 = (query) => {
    setValue2(query);
    setData2(queries.filter(item => filter(item, query)));
  };

  const renderOption2 = (item, index) => (
    <AutocompleteItem
      key={index}
      title={item.title}
    />
  );
  const renderCloseIcon = (props) => (
    <TouchableWithoutFeedback onPress={clearInput2}>
      {/* <Ionicons name='md-close' size={18}
        color="#8F9BB3" /> */}
    </TouchableWithoutFeedback>
  );

  return (
    <>
      <Text
        style={[styles.text, globalStyles.marginBottom15, globalStyles.marginTop15]}>
        Search by country
            </Text>

      <Layout style={styles.blockSearch}>
        <View style={{ flex: 1, marginRight: 10, alignItems: 'stretch'}}>

          <Autocomplete
            placeholder='Place your Text'
            value={value2}
            placement={placement}
            accessoryRight={renderCloseIcon}
            onSelect={onSelect2}
            onChangeText={onChangeText2}
            size="small"

          >
            {data2 && data2.map(renderOption2)}
          </Autocomplete>



        </View>
        <View style={{ flex: 1 }}>
          <Select
            placeholder='Default'
            value={displayValue}
            onSelect={onSelect1}
            size="small"
            style={{ margin: 0, padding: 0, alignItems: 'stretch' }}
          >
            {dataSelect.map((item, index) => {
              return (
                <Fragment key={`select${item.filter}`}>
                  <SelectItem key={item.title} title={item.title} />
                </Fragment>
              )
            })}
          </Select>
        </View>
      </Layout>
    </>
  )
}

export default Search
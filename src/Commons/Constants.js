import { Dimensions } from 'react-native'

export const { width, height } = Dimensions.get('window')
export const ASPECT_RATIO = width / height
export const LATIITUDE = 10.7169273
export const LONGIITUDE = 106.6531285
export const LATIITUDE_DELTA = 0.0922 * 10
export const LONGIITUDE_DELTA = LATIITUDE_DELTA * ASPECT_RATIO 
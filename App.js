

import React from 'react';
import { Text, View } from 'react-native';
import AppHeader from './src/Navigation/AppHeader';
import { Provider } from 'react-redux'
import store from './src/Store'
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';
// import { SafeAreaProvider } from 'react-native-safe-area-context'
// import SafeAreaView from 'react-native-safe-area-view';

const App = () => {
  return (
    <>
      <Provider store={store}>
        {/* <SafeAreaProvider style={{ flex: 1 }}> */}
          <ApplicationProvider {...eva} theme={eva.light}>
            {/* <SafeAreaView style={{ flex: 1 }}> */}
            <AppHeader />
            {/* <View style={{ flex: 1 }}>
              <Text>
                Look, I'm safe! Not under a status bar or notch or home indicator or
                anything! Very cool
              </Text>
            </View> */}
            {/* </SafeAreaView> */}

          </ApplicationProvider>
        {/* </SafeAreaProvider> */}
      </Provider>

      {/* <View style={{ flex: 1 }}>
        <AppHeader />
      </View> */}
    </>
  );
};



export default App;
